package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.BusLine;
import eu.drus.jpa.unit.api.Bootstrapping;
import eu.drus.jpa.unit.api.JpaUnit;
import eu.drus.jpa.unit.api.TransactionMode;
import eu.drus.jpa.unit.api.Transactional;
import org.flywaydb.core.Flyway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(JpaUnit.class)
@DisplayName("Lanzamos Flyway para inicializar la BD")
@Transactional(TransactionMode.ROLLBACK)
public class DatabaseRepositoryFlywayTest extends UtilsJUnit {

    @PersistenceContext(unitName = "test-maria")
    private EntityManager entityManager;
    BusBusLineDatabaseRepository busLineDatabaseRepository = new BusBusLineDatabaseRepository();

    @Bootstrapping
    public static void prepareDataBase(final DataSource ds) {
        // creates db schema and puts some data
        Flyway flyway = Flyway.configure().dataSource(ds).load();
        flyway.clean();
        flyway.migrate();
    }

    @BeforeEach
    public void setUp() {
        busLineDatabaseRepository.entityManager = entityManager;
    }

    @Test
    @DisplayName("Comprobamos si se han insertado los datos")
    void should_initialize_json() {
        List<BusLine> busLines = busLineDatabaseRepository.findAll();
        assertEquals(5, busLines.size());
    }
}