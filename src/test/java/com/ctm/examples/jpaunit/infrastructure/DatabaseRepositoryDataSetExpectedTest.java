package com.ctm.examples.jpaunit.infrastructure;

import eu.drus.jpa.unit.api.ExpectedDataSets;
import eu.drus.jpa.unit.api.JpaUnit;
import eu.drus.jpa.unit.api.TransactionMode;
import eu.drus.jpa.unit.api.Transactional;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@ExtendWith(JpaUnit.class)
@Transactional(TransactionMode.COMMIT)
@DisplayName("Ejecutamos commit en el result")
class DatabaseRepositoryDataSetExpectedTest extends UtilsJUnit {

    @PersistenceContext(unitName = "test-maria")
    private EntityManager entityManager;
    BusBusLineDatabaseRepository busLineDatabaseRepository = new BusBusLineDatabaseRepository();

    @BeforeEach
    public void setUp() {
        busLineDatabaseRepository.entityManager = entityManager;
    }

    @Test
    @DisplayName("Insertamos datos y comprobamos que sean los resultados esperados")
    @ExpectedDataSets(value = "dataset/expected-data.json", orderBy = {"BusLine.id"})
    void should_create_new_line() {
        busLineDatabaseRepository.create(generateLine("Linea 1", "Roja", 1));
        busLineDatabaseRepository.create(generateLine("Linea 2", "Azul", 2));
        busLineDatabaseRepository.create(generateLine("Linea 3", "Verde", 3));
        busLineDatabaseRepository.create(generateLine("Linea 4", "Amarilla", 4));
    }

}