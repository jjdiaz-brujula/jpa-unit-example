package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.BusLine;
import com.ctm.examples.jpaunit.domain.Stop;
import com.ctm.examples.jpaunit.domain.Ticket;

import java.util.Calendar;

public class UtilsJUnit {
    BusLine generateLine() {
        BusLine busLine = new BusLine();
        busLine.setName("Portopí - Palau de Congressos");
        busLine.setColor("Verde");
        busLine.setNumber(1);
        return busLine;
    }

    BusLine generateLine(String name, String color, Integer number) {
        BusLine busLine = new BusLine();
        busLine.setName(name);
        busLine.setColor(color);
        busLine.setNumber(number);
        return busLine;
    }

    Stop generateStop(Integer busLineId, int position) {
        Stop stop = new Stop();
        stop.setName("Parada " + position);
        stop.setBusLineId(busLineId);
        stop.setPosition(position);
        return stop;
    }

    Ticket generateTicket(Stop stop) {
        Ticket ticket = new Ticket();
        ticket.setCreateDate(Calendar.getInstance().getTime());
        ticket.setStop(stop);
        return ticket;
    }
}
