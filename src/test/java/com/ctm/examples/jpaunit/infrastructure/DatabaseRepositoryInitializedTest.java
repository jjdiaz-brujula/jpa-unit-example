package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.BusLine;
import eu.drus.jpa.unit.api.Cleanup;
import eu.drus.jpa.unit.api.CleanupPhase;
import eu.drus.jpa.unit.api.CleanupStrategy;
import eu.drus.jpa.unit.api.InitialDataSets;
import eu.drus.jpa.unit.api.JpaUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(JpaUnit.class)
@DisplayName("Inicializamos la BD")
class DatabaseRepositoryInitializedTest extends UtilsJUnit {

    @PersistenceContext(unitName = "test-h2")
    private EntityManager entityManager;
    BusBusLineDatabaseRepository busLineDatabaseRepository = new BusBusLineDatabaseRepository();

    @BeforeEach
    public void setUp() {
        busLineDatabaseRepository.entityManager = entityManager;
    }

    @Test
    @InitialDataSets("dataset/initial-data.json")
    @DisplayName("Comprobamos si se han insertado los datos con JSON")
    void should_initialize_json() {
        List<BusLine> busLines = busLineDatabaseRepository.findAll();
        assertEquals(4, busLines.size());
    }

    @Test
    @InitialDataSets("dataset/initial-data.xml")
    @DisplayName("Comprobamos si se han insertado los datos con XML")
    @Cleanup(phase = CleanupPhase.AFTER, strategy = CleanupStrategy.USED_ROWS_ONLY)
    void should_initialize_xml() {
        List<BusLine> busLines = busLineDatabaseRepository.findAll();
        assertEquals(3, busLines.size());
    }

}