package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.BusLine;
import com.ctm.examples.jpaunit.domain.Stop;
import com.ctm.examples.jpaunit.domain.Ticket;
import eu.drus.jpa.unit.api.JpaUnit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceProperty;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

@ExtendWith(JpaUnit.class)
@DisplayName("Cambiamos la url de la BD")
class DatabaseRepositoryChangedUrlTest extends UtilsJUnit {

    @PersistenceContext(unitName = "test-h2", properties = {@PersistenceProperty(name = "javax.persistence.jdbc.url", value = "jdbc:h2:mem:test2")})
    private EntityManager entityManager;
    BusBusLineDatabaseRepository busLineDatabaseRepository = new BusBusLineDatabaseRepository();
    StopDatabaseRepository       stopDatabaseRepository    = new StopDatabaseRepository();
    TicketDatabaseRepository     ticketDatabaseRepository  = new TicketDatabaseRepository();

    @BeforeEach
    public void setUp() {
        busLineDatabaseRepository.entityManager = entityManager;
        stopDatabaseRepository.entityManager = entityManager;
        ticketDatabaseRepository.entityManager = entityManager;
    }

    @Test
    @DisplayName("Inicializar linea y crear ticket")
    void should_create_new_line() {
        BusLine busLine = generateLine();

        busLineDatabaseRepository.create(busLine);

        stopDatabaseRepository.create(generateStop(busLine.getId(), 1));
        stopDatabaseRepository.create(generateStop(busLine.getId(), 2));
        stopDatabaseRepository.create(generateStop(busLine.getId(), 3));

        Stop stop = generateStop(busLine.getId(), 4);
        stopDatabaseRepository.create(stop);
        stopDatabaseRepository.entityManager.flush();

        List<Stop> stops = stopDatabaseRepository.findAll();

        Ticket ticket = generateTicket(stop);
        ticketDatabaseRepository.create(ticket);
        assertAll(() -> assertEquals(1, busLine.getId().intValue()),
                () -> assertEquals(4, stops.size()),
                () -> assertEquals(4, stop.getId().intValue()));
    }

    @Test
    @DisplayName("Crear nueva linea por segunda vez")
    void should_create_new_line_again() {
        BusLine busLine = generateLine();
        busLineDatabaseRepository.create(busLine);
        assertNotEquals(1, busLine.getId().intValue());
    }
}