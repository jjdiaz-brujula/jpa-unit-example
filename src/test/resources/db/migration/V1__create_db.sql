CREATE TABLE `BusLines`
(
    `id`     INT(11)     NOT NULL AUTO_INCREMENT,
    `color`  VARCHAR(20) NOT NULL,
    `name`   VARCHAR(50) NOT NULL,
    `number` INT(11)     NULL DEFAULT NULL,
    PRIMARY KEY (`id`)
);

CREATE TABLE `Stops`
(
    `id`        INT(11)     NOT NULL AUTO_INCREMENT,
    `busLineId` INT(11)     NULL DEFAULT NULL,
    `name`      VARCHAR(50) NOT NULL,
    `position`  INT(11)     NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `FK_Stops_BusLines` (`busLineId`),
    CONSTRAINT `FK_Stops_BusLines` FOREIGN KEY (`busLineId`) REFERENCES `BusLines` (`id`)
);
CREATE TABLE `Tickets`
(
    `id`         INT(11)     NOT NULL AUTO_INCREMENT,
    `createDate` DATETIME(6) NULL DEFAULT NULL,
    `stop_id`    INT(11)     NULL DEFAULT NULL,
    PRIMARY KEY (`id`),
    INDEX `FK_Tickets_Stops` (`stop_id`),
    CONSTRAINT `FK_Tickets_Stops` FOREIGN KEY (`stop_id`) REFERENCES `Stops` (`id`)
);

INSERT INTO `BusLines` (`name`, `color`, `number`)
VALUES ('Linea 1', 'Roja', 1),
       ('Linea 2', 'Azul', 2),
       ('Linea 3', 'Verde', 3),
       ('Linea 4', 'Verde', 4),
       ('Linea 5', 'Verde', 5);

