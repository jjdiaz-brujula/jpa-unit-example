package com.ctm.examples.jpaunit.domain;

import java.util.List;

public interface StopRepository {
    void create(Stop stop);

    List<Stop> findAll();

    Stop findById(Integer id);
}
