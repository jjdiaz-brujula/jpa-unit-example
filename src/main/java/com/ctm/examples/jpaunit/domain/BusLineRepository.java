package com.ctm.examples.jpaunit.domain;

import java.util.List;

public interface BusLineRepository {
    void create(BusLine busLine);

    void update(BusLine busLine);

    List<BusLine> findAll();

    BusLine findById(Integer id);
}
