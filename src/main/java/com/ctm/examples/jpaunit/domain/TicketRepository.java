package com.ctm.examples.jpaunit.domain;

import java.util.List;

public interface TicketRepository {
    void create(Ticket stop);

    List<Ticket> findAll();

    Ticket findById(Integer id);
}
