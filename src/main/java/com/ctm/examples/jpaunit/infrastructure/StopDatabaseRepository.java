package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.Stop;
import com.ctm.examples.jpaunit.domain.StopRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class StopDatabaseRepository implements StopRepository {
    @PersistenceContext
    EntityManager entityManager;


    @Override
    public void create(Stop stop) {
        entityManager.persist(stop);
    }

    @Override
    public List<Stop> findAll() {
        return entityManager.createQuery("Select s from Stop s").getResultList();
    }

    @Override
    public Stop findById(Integer id) {
        return (Stop) entityManager.createQuery("SELECT s FROM Stop s WHERE id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
}
