package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.BusLine;
import com.ctm.examples.jpaunit.domain.BusLineRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class BusBusLineDatabaseRepository implements BusLineRepository {
    @PersistenceContext
    EntityManager entityManager;


    @Override
    public void create(BusLine busLine) {
        entityManager.persist(busLine);
    }

    @Override
    public void update(BusLine busLine) {
        entityManager.persist(busLine);
    }

    @Override
    public List<BusLine> findAll() {
        return entityManager.createQuery("Select b from BusLine b").getResultList();
    }

    @Override
    public BusLine findById(Integer id) {
        return (BusLine) entityManager.createQuery("SELECT b FROM BusLine b WHERE id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
}
