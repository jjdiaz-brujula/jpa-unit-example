package com.ctm.examples.jpaunit.infrastructure;

import com.ctm.examples.jpaunit.domain.Ticket;
import com.ctm.examples.jpaunit.domain.TicketRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class TicketDatabaseRepository implements TicketRepository {
    @PersistenceContext
    EntityManager entityManager;

    @Override
    public void create(Ticket ticket) {
        entityManager.persist(ticket);
    }

    @Override
    public List<Ticket> findAll() {
        return entityManager.createQuery("Select t from Ticket t").getResultList();
    }

    @Override
    public Ticket findById(Integer id) {
        return (Ticket) entityManager.createQuery("SELECT t FROM Ticket t WHERE id = :id")
                .setParameter("id", id)
                .getSingleResult();
    }
}
