docker-compose -p test down
docker-compose -p test rm -v --force
docker volume prune --force
docker-compose build
docker-compose -p test up --force-recreate -d