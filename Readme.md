#JPA Unit Example
Un pequeño ejemplo de como utilizar esta librería para testar las implementaciones de JPA de nuestros proyectos.

Es válido para realizar test tanto con JUnit 4, como con JUnit5.

La url donde poder encontrarla es en [JPA Unit](https://github.com/dadrus/jpa-unit/)

Las dependencias mediante maven son:
```xml
<dependency>
    <groupId>com.github.dadrus.jpa-unit</groupId>
    <artifactId>jpa-unit5</artifactId>
    <scope>test</scope>
</dependency>
```

Esta es necesaria si se quiere inicializar las bases de datos de forma externa:
```xml

<dependency>
    <groupId>com.github.dadrus.jpa-unit</groupId>
    <artifactId>jpa-unit-rdbms</artifactId>
    <scope>test</scope>
</dependency>
```

##@PersistentContext
Gracias a esta anotación, podrás instanciar tu EntityManager definiendo el unitName o incluso modificando o añadiendo propiedades de configuración
```java
@PersistenceContext(unitName = "test-maria", properties = {@PersistenceProperty(name = "hibernate.hbm2ddl.auto", value = "create-drop")})
EntityManager em;
```

##Control de transacciones
Con esta librería nos permite controlar el tipo de transacciones. Esta anotación se podrá hacer a nivel de clase, la cual se ejecutará al final de todos los test o en solo un test
```java
@Transactional(TransactionMode.COMMIT)
```
Tiene tres opciones:
* COMMIT
* DISABLE
* ROOLBACK

Además, si se desactiva las transacciones, nos permite mediante una serie de métodos poder ejecutar transacciones.
* newTransaction(EntityManager em): Nos permite crear una nueva transacción.
* flushContextOnCommit(boolean flag): Nos permite vaciar el entityManager despues de realizar el commit.
* clearContextOnCommit(boolean flag): Nos permite borrar el entityManager despues de realizar el commit.
* execute(<Expression>): Ejecuta la acción y la incluye dentro de una nueva transacción. Si esta acción incluye la devolución de datos, esta se realizaría en la misma llamada. Teniendo el siguiente comportamiento:
  * Antes de la ejecución de <Expression>: Si existe una transacción activa, se confirma y se genera una nueva transacción. Si no, se generaría solo la nueva.
  * Después de la ejecución de <Expression>: La transacción de <Expression> se confirma. Si se estaba ejecutando una transacción activa y se confirmó antes de que se iniciara la transacción de ajuste <Expresion>, se inicia una nueva transacción.

`Existe un pequeño problema con esto, al añadir la dependencia jpa-unit-rdbms aunque esté con TransactionMode.COMMIT los datos son eliminados al finalizar el test.`  

## Inicialización de datos
Esta se puede hacer a nivel  de clase o @Test y es obligatorio incluir la dependecia **jpa-unit-rdbms**. Para poder utilizar esta funcinalidad es necesario incluir la siguiene anotación:
```java
 @InitialDataSets("dataset/initial-data.json")
```
También cabría la posibilidad de poder testear si la BD generada con FlyWay se ha generado correctmente, ya que permite realizar una migración a nivel de Clase de la BD utilizando la anotación: 
```java
@Bootstrapping
public static void prepareDataBase(final DataSource ds) {
    // creates db schema and puts some data
    Flyway flyway = Flyway.configure().dataSource(ds).load();
    flyway.clean();
    flyway.migrate();
}
``` 
## Verificación de Datos
Otra funcionalidad que incluye esta librería, es la de poder realizar test con datos esperados después de su ejecución con la siguiente anotación:
```java
 @ExpectedDataSets("dataset//expected-data.json")
```
Podríamos añadir también las opciones de excluir campos y definir un orden de compración:
```java
@Test
@ExpectedDataSets(value = "datasets/expected-data.json", excludeColumns = {
        "ID", "DEPOSITOR_ID", "ACCOUNT_ID", "VERSION"
}, orderBy = {
        "CONTACT_DETAIL.TYPE", "ACCOUNT_ENTRY.TYPE"
})
```
##Limpiar la BD
Permite realizar limpieza de datos o tablas, ya sea antes o después de a ejecución del test. Incluyendo la posibilidad de realizar la limpieza a nivel de registros o tabla que se han utilizado en ese test. 

Además, esta limpieza tambien puede definirse en un script que se ejecutará antes/después de la ejecución del test.
```java
@Test
@CleanupUsingScripts(phase = CleanupPhase.AFTER, value = "scripts/delete-all.script")
public void otherTest() {
    // your code here
}
```
## Control de Caché de segundo nivel
Proporciona la posiblidad de testear la caché JPA L2. Como en la mayoría de las anotaciones, se puede usar a nivel de clase o de Test. Tendría las opciones de:
* BEFORE: La caché L2 se limpia antes de la ejecución.
* AFTER: La caché L2 se limpia después de la ejecución. Es el valor predeterminado.
* NONE: La limpieza de la caché L2 está deshabilitada.
 ```java
@RunWith(JpaUnitRunner.class)
@CleanupCache(TransactionMode.BEFORE)
public class MyTest {

    @PersistenceContext(unitName = "my-test-unit")
    private EntityManager manager;
	
    @Test
    public void someTest() {
        // your code here
    }
}
```
## DataSets
Gracias a esta funcionalidad podemos usar diferentes formatos para la inicialización de datos o la comprobación de datos esperados.
Según la la documentación oficial, se permiten los siguientes formatos: XML, YAML, JSON, XSL(X) y CSV

`Yo solo he probado JSON, XML y YAML, aunque YAML no he conseguido que funcionara`

## Otros aspectos a considerar
Además de todas las posibilidades anteriores, provée la posiblidad de tener una conexión con MongoDB, Neo4j, CDI, Cucumber y Concordion 